import { Request, Response } from "express-serve-static-core";

export default class HomeController {
    static index(req: Request, res: Response): void {
        const db = req.app.locals.db;
        const descrip = db.prepare("SELECT * FROM devWebDescrip ").all();
        res.render('pages/index', {
            arrayDescrip: descrip,
        });
    }
}



