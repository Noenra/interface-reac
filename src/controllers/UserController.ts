import { Request, Response } from "express-serve-static-core";
import CompetencesController from "./CompetencesController";
import HomeController from "./HomeController";
export default class UserController {
//affiche la page register
    static RegForm(req: Request, res: Response): void {
        res.render('pages/register');
    }

//stock les donnees d'utilisateur dans la base de donnee    
    static register(req: Request, res: Response): void {
        const db = req.app.locals.db;

        // console.log(req.body);


        db.prepare('INSERT INTO user ( "username", "email" , "password") VALUES ( ?, ?, ?)').run(req.body.username, req.body.email, req.body.password);

        UserController.LogForm(req, res);
    }
// quand le process d'eregistrement est fini il est evoie vers la page de login    

    static LogForm(req: Request, res: Response): void {
        res.render('pages/login');
    }
// affiche la page login et demands utilisateur pour un nom d'utilisateur et un mots de passe    

    static login(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const username = req.body.username;
        const password = req.body.password;

// le nom d'utilisateur et mot de passe est comparee avec les donnees du database dans le tableau user et s'il existe est il correspond il est dirigee vers le index si no vers le login
        const user = db.prepare('SELECT * FROM user WHERE username = ? AND password = ?').get(username, password);
        console.log('user exists ?: ', user)

        if (user) {
            CompetencesController.competences(req, res);
        }
        else {
            UserController.LogForm(req, res);
        }
    }
}











