(
    "Développer la partie back-end d’une application web
ou web mobile en intégrant les recommandations de
sécurité",
    "Créer une base de données",
    "Description de la compétence – processus de mise en œuvre
À partir d’une demande client nécessitant le stockage de données, organiser les données et définir un
schéma physique. A l’aide d’un SGBD, écrire et exécuter le script de création de la base de données,
insérer les données de test, définir les droits d' utilisation et prévoir les procédures de sauvegarde et de restauration de la base de données de test.Conformément à l ’ état de l ’ art de la sécurité et aux exigences de sécurité identifiées,
    exprimer le besoin de sécurité du SGDB afin de permettre l ’ élaboration d ’ une base de données sécurisée.Rechercher,
    en utilisant de la documentation en langue française ou anglaise,
    des solutions pertinentes pour la résolution de problèmes techniques et de nouveaux usages,
    notamment des bases de données non relationnelles.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Les bases de données relationnelles sont utilisées dans les applications traditionnelles où les données sont centralisées sur un serveur.Les données sont amenées à être souvent mises à jour.Les bases de données non relationnelles sont utilisées dans les cas de traitement de données massives à l 'échelle du Web. Les données sont essentiellement lues, il y a peu de mises à jour.
Dans le cas d' une demande d 'évolution applicative et à partir d' une base de données existante,
    il s ’ agira de modifier le schéma physique.Les différents scripts de création de base de données,
    de gestion des droits,
    d 'insertion de données et
d' exécution de sauvegarde et restauration sont exécutés sur un serveur supportant un système de gestion de base de données.Les besoins de sécurité du SGBD sont exprimés par le développeur et le paramétrage est effectué par un administrateur de bases de données."
),
(
    " Développer la partie back -
end d ’ une application web ou web mobile en intégrant les recommandations de sécurité ",
    " Développer les composants d ’ accès aux données ' , ' Description de la compétence – processus de mise en œuvre À partir du dossier de conception technique et d ’ une bibliothèque d ’ objets spécialisés dans l ’ accès aux données,
coder,
tester et documenter les composants d 'accès aux données stockées dans une base de
données afin d’opérer des sélections et des mises à jour de données nécessaires à une application
informatique et de façon sécurisée.
Rechercher, éventuellement en langue anglaise, des solutions innovantes et pertinentes pour la résolution
de problèmes techniques et de nouveaux usages, notamment d' accès aux données non relationnelles.Pratiquer une veille technologique sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa recherche ou de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s 'exerce seul ou en équipe et concerne le développement de la partie persistance de
l' application.Le développement des composants s ’ effectue à partir d 'un environnement de développement
intégré, supportant un langage objet en liaison avec une base de données relationnelles ou non
relationnelles. L' accès aux données passe par l 'utilisation d’un logiciel d’interface (middleware).
Les données peuvent être dans un contexte de base de données relationnelles ou non relationnelles."
),
(
    "Développer la partie back-end d’une application web
ou web mobile en intégrant les recommandations de
sécurité",
    "Développer la partie back-end d’une application web ou web mobile",
    "Description de la compétence – processus de mise en œuvre
À partir des fonctionnalités décrites dans le dossier de conception technique, et dans le respect des
bonnes pratiques de développement et de sécurisation d' application web,
    coder,
    tester et documenter les traitements côté serveur,
    afin d ’ assurer la collecte,
    le traitement et la restitution d ’ informations numériques.Publier l ’ application web sur un serveur.Pratiquer une veille technologique,
    y compris en anglais,
    pour résoudre un problème technique ou mettre en œuvre une nouvelle fonctionnalité ainsi que pour s ’ informer sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s 'exerce seul ou en équipe. Le développement d' une application web s ’ effectue à partir d 'un environnement de développement intégré, supportant les différents langages serveur.
L’application web est optimisée pour les équipements mobiles à travers la mise en place de mécanismes
asynchrones de chargement et restitution de données (Ajax, ...)."
),
(
    "Développer la partie back-end d’une application web
ou web mobile en intégrant les recommandations de
sécurité",
    "Elaborer et mettre en œuvre des composants dans une
application de gestion de contenu ou e-commerce",
    "Description de la compétence – processus de mise en œuvre
À partir du cahier des charges fonctionnel et du système de gestion de contenu ou d' e - commerce,
    dans le respect des bonnes pratiques de développement,
    de la solution logicielle,
    intégrer ou coder,
    tester et documenter des modules complémentaires afin de rendre le site web adapté aux besoins des utilisateurs,
    en respectant à chaque étape l ’ état de l ’ art de la sécurité informatique.Publier l ’ application web sur un serveur.Pratiquer une veille technologique,
    y compris en anglais,
    pour résoudre un problème technique ou mettre en œuvre une nouvelle fonctionnalité ainsi que pour s ’ informer sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa veille avec ses pairs.Contexte(s) professionnel(s) de mise en œuvre Cette compétence s 'exerce seul ou en équipe. Le développement d' une application web s ’ effectue à partir d 'un environnement de développement intégré, supportant les différents langages serveur.'
);

INSERT INTO " Criteres " ( " Content ", " Competences_ID ")
VALUES (
        'La maquette prend en compte les spécificités fonctionnelles décrites dans les cas d' utilisation ou les scénarios utilisateur ' , ' L 'enchaînement des écrans est formalisé par un schéma', 1)
        ('La maquette et l' enchaînement des écrans sont validés par l ’ utilisateur ',1)
        (' La maquette respecte la charte graphique de l ’ entreprise ',)
        (' La maquette est conforme à l 'expérience utilisateur et à l’équipement ciblé',1)
        ('La maquette respecte les principes de sécurisation d’une interface utilisateur',1)
        ('La maquette prend en compte les exigences de sécurité spécifiques de l’application',1)
        ('Le contenu de la maquette est rédigé, en français ou en anglais, de façon adaptée à l’interlocuteur et sans, faute',1)

        (" L ' interface est conforme à la maquette de l ' application ", 2)
        (" Les pages web respectent la charte graphique de l ' entreprise ",2)
        ("Les bonnes pratiques de structuration et de sécurité sont respectées y compris pour le web mobile",2) 
        ("Les pages web sont accessibles depuis les navigateurs ciblés y compris depuis un mobile ",2)
        ("Les pages web s ’ adaptent à la taille de l ’ écran ",2)
        ("Les pages web sont optimisées pour le web mobile",2) 
        ("Le site respecte les règles de référencement naturel Les pages web sont publiées sur un serveur ",2)
        ("L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
        en français ou en anglais, est comprise (sans contre - sens,...) 
        ("La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité 
        ("Le partage du résultat de recherche et de veille est effectué,
        oralement ou par écrit, avec ses pairs '
) (
    ' Les pages web respectent la charte graphique de l ' entreprise Les pages web sont conformes à l ’ expérience utilisateur y compris pour l ’ expérience mobile.L ' architecture de l ' application répond aux bonnes pratiques de développement et de sécurisation d ' application web L ’ application web est optimisée pour les équipements mobiles Le code source est documenté ou auto - documenté L ' application web est publiée sur un serveur Les tests garantissent que les pages web répondent aux exigences décrites dans le cahier des charges Les tests de sécurité suivent un plan reconnu par la profession L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
    en français ou en anglais,
    est comprise (sans contre - sens,...) La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs '
    ) (
        ' Le site est installé et paramétré conformément au besoin client Les comptes utilisateurs sont créés avec leurs droits et rôles dans le respect des règles de sécurité La structure du site est conforme au besoin client L ’ aspect visuel respecte la charte graphique du client et est adapté à l ’ équipement de l ’ utilisateur Le site est publié sur un serveur Le site respecte les règles de référencement naturel L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
    en français ou en anglais,
    est comprise (sans contre - sens,...) La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs '
    ) (
        ' La base de données est conforme au schéma physique Le script de création de bases de données s ’ exécute sans erreurs Le script d ’ insertion des données de test s ’ exécute sans erreurs La base de données est disponible avec les droits d ' accès prévus La base de données de test peut être restaurée en cas d ' incident Les besoins de sécurité du SGBD sont exprimés selon l ’ état de l ’ art et les exigences de sécurité identifiées L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
    en français ou en anglais,
    est comprise (sans contre - sens,...) La démarche de recherche permet de trouver une solution à un problème technique La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs '
    ) (
        ' Les traitements relatifs aux manipulations des données répondent aux fonctionnalités décrites dans le dossier de conception technique Un test unitaire est associé à chaque composant,
    avec une double approche fonctionnelle et sécurité Le code source des composants est documenté ou auto - documenté Les composants d ’ accès à la base de données suivent les règles de sécurisation reconnues La sécurité des composants d ’ accès se fonde sur les mécanismes de sécurité du SGBD L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
    en français ou en anglais,
    est comprise (sans contre - sens,...) La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles Le partage du résultat de veille est effectué oralement ou par écrit avec ses pairs '
    ) (
        ' Les bonnes pratiques de développement sont respectées Les composants serveur contribuent à la sécurité de l ’ application Le code source des composants est documenté ou auto - documenté Les tests garantissent que les traitements serveurs répondent aux fonctionnalités décrites dans le dossier de conception technique Les tests de sécurité suivent un plan reconnu par la profession L ' application web est publiée sur un serveur L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
        y compris en anglais,
        est comprise (sans contre - sens,...) La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles Le partage du résultat de recherche et de veille est effectué,
        oralement ou par écrit,
        avec ses pairs '
) (
    ' Les bonnes pratiques de développement objet sont respectées Les composants complémentaires ou réalisés s ’ intègrent dans l ’ environnement de l ’ application Les composants serveur contribuent à la sécurité de l ’ application Le code source des composants est documenté ou auto - documenté Les tests garantissent que les traitements serveurs répondent aux fonctionnalités décrites dans le cahier des charges Les tests de sécurité suivent un plan reconnu par la profession L ' application web est publiée sur un serveur L ’ objet de la recherche est exprimé de manière précise en langue française ou anglaise La documentation technique liée aux technologies associées,
    y compris en anglais,
    est comprise (sans contre - sens,...) La démarche de recherche permet de résoudre un problème technique ou de mettre en œuvre une nouvelle fonctionnalité La veille sur les vulnérabilités connues permet d ’ identifier et corriger des failles potentielles Le partage du résultat de recherche et de veille est effectué,
    oralement ou par écrit,
    avec ses pairs '
    )
    INSERT INTO "users" ("username" , "email", "password", "created_at", "deleted_at" , "modified_at")
VALUES (